@ECHO OFF
setlocal

set PROJECT_HOME=%~dp0
set DEMO=Red Hat Process Automation Manager Easy Install
set AUTHORS=Andrew Block, Eric D. Schabell, Duncan Doyle
set PROJECT=git@gitlab.com:redhatdemocentral/crc-rhpam-install-demo.git
set SUPPORT_DIR=%PROJECT_HOME%\support
set OC_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.7.18/"

REM Adjust these variables to point to an OCP instance.
set OPENSHIFT_ADMIN_PASS=
set OPENSHIFT_ADMIN=kubeadmin
set OPENSHIFT_USER=developer
set OPENSHIFT_PWD=developer
set HOST_IP=api.crc.testing   # set with OCP instance hostname or IP.
set HOST_APPS=apps-crc.testing
set HOST_PORT=6443
set OCP_APP=rhpam
set OCP_PRJ=appdev-in-cloud

set KIE_ADMIN_USER=erics
set KIE_ADMIN_PWD=redhatpam1!

REM Waiting max 8 min various container functions to startup.
set DELAY=540

REM import container functions.
call %SUPPORT_DIR%\container-functions.bat

REM wipe screen.
cls

echo.
echo ######################################################################
echo ##                                                                  ##   
echo ##  Setting up the %DEMO%  ##
echo ##                                                                  ##   
echo ##             ####  ##### ####     #   #  ###  #####               ##
echo ##             #   # #     #   #    #   # #   #   #                 ##
echo ##             ####  ###   #   #    ##### #####   #                 ##
echo ##             #  #  #     #   #    #   # #   #   #                 ##
echo ##             #   # ##### ####     #   # #   #   #                 ##
echo ##                                                                  ##
echo ##           ####  ####   ###   #### #####  ####  ####              ##
echo ##           #   # #   # #   # #     #     #     #                  ##
echo ##           ####  ####  #   # #     ###    ###   ###               ##
echo ##           #     #  #  #   # #     #         #     #              ##
echo ##           #     #   #  ###   #### ##### ####  ####               ##
echo ##                                                                  ##
echo ##   ###  #   # #####  ###  #   #  ###  ##### #####  ###  #   #     ##
echo ##  #   # #   #   #   #   # ## ## #   #   #     #   #   # ##  #     ##
echo ##  ##### #   #   #   #   # # # # #####   #     #   #   # # # #     ##
echo ##  #   # #   #   #   #   # #   # #   #   #     #   #   # #  ##     ##
echo ##  #   # #####   #    ###  #   # #   #   #   #####  ###  #   #     ##
echo ##                                                                  ##
echo ##           #   #  ###  #   #  ###  ##### ##### ####               ##
echo ##           ## ## #   # ##  # #   # #     #     #   #              ##
echo ##           # # # ##### # # # ##### #  ## ###   ####               ##
echo ##           #   # #   # #  ## #   # #   # #     #  #               ##
echo ##           #   # #   # #   # #   # ##### ##### #   #              ##
echo ##                                                                  ## 
echo ##                 #### #      ###  #   # ####                      ##
echo ##            #   #     #     #   # #   # #   #                     ##
echo ##           ###  #     #     #   # #   # #   #                     ##
echo ##            #   #     #     #   # #   # #   #                     ##
echo ##                 #### #####  ###   ###  ####                      ##
echo ##                                                                  ##   
echo ##  brought to you by,                                              ##   
echo ##             %AUTHORS%         ##
echo ##                                                                  ##   
echo ##  %PROJECT%    ##
echo ##                                                                  ##   
echo ######################################################################
echo.


REM Validate OpenShift
set argTotal=0

for %%i in (%*) do set /A argTotal+=1

if %argTotal% EQU 1 (

    call :validateIP %1 valid_ip

	if !valid_ip! EQU 0 (
	    echo OpenShift host given is a valid IP...
	    set HOST_IP=%1
		echo.
		echo Proceeding with OpenShift host: !HOST_IP!...
	) else (
		echo Please provide a valid IP that points to an OpenShift installation...
		echo.
        call :printDocs
	)

)

if %argTotal% GTR 1 (
    call :printDocs
)

if %argTotal% EQU 0 (
	if [%HOST_IP%] == [] (
		call :printDocs
	)

	echo.
	echo Assuming you set a valid host, so proceeding with: %HOST_IP%
	echo.
)

REM make some checks first before proceeding.	
call where oc version --client >nul 2>&1
if  %ERRORLEVEL% NEQ 0 (
	echo OpenShift command line tooling is required but not installed yet... download here: %OCP_URL%
	echo.
	GOTO :EOF
)

echo OpenShift commandline tooling is installed...
echo.
IF "%OPENSHIFT_ADMIN_PASS%" == "" (
  echo.
	echo You need to update the variable OPENSHIFT_ADMIN_PASS with a valide admin password
  echo to login to your OpenShift cluster. If using CodeReady Containers, it was provided
  echo in the start up console. This variable is found at the top of the init script.
  echo After updating, please run this init script again.
  echo.
	GOTO :EOF
)

echo Logging in to OpenShift as %OPENSHIFT_USER%...
echo.
call oc login %HOST_IP%:%HOST_PORT% --password="%OPENSHIFT_PWD%" --username="%OPENSHIFT_USER%"

if not "%ERRORLEVEL%" == "0" (
  echo.
	echo Error occurred during 'oc login' %OPENSHIFT_USER% command!
	echo.
	GOTO :EOF
)

echo.
echo Creating a new project...
echo.
call oc new-project %OCP_PRJ%

echo Logging in to OpenShift as %OPENSHIFT_ADMIN%...
echo.
call oc login %HOST_IP%:%HOST_PORT% --password="%OPENSHIFT_ADMIN%" --username="%OPENSHIFT_ADMIN_PASS%"

if not %ERRORLEVEL% == 0 (
  echo.
	echo Error occurred during 'oc login' %OPENSHIFT_ADMIN% command!
	echo.
	GOTO :EOF
)

echo.
echo Creating operator group for the businessautomation operator installation...
echo.
call oc apply -f %SUPPORT_DIR%\create-operatorgroup.yaml

if not "%ERRORLEVEL%" == "0" (
	echo.
  echo Error occurred during 'oc apply' create operator group command.
	echo.
  GOTO :EOF
)

echo.
echo Setting subscription information for the businessautomation operator installation...
echo.
call oc apply -f %SUPPORT_DIR%\sub-operator.yaml

if not "%ERRORLEVEL%" == "0" (
	echo.
  echo Error occurred during 'oc apply' subscription command.
  echo.
  GOTO :EOF
)

echo Logging in to OpenShift as %OPENSHIFT_USER%...
echo.
call oc login %HOST_IP%:%HOST_PORT% --password="%OPENSHIFT_PWD%" --username="%OPENSHIFT_USER%"

if not "%ERRORLEVEL%" == "0" (
  echo.
	echo Error occurred during 'oc login' command!
	echo.
	GOTO :EOF
)

echo.
echo Starting instance of operator...
echo.

if call :instanceReady (
	echo The instance has started using the operator...
	echo.
else
	echo Exiting now with CodeReady Container started, but business automation operator"
	echo did not finish installing an instance inside of the set time. Can't complete"
	echo the installation of an instance for this project."
	echo.
	GOTO :EOF
)

if call :routeReady "rhpamcentr" (
	echo The business central insecure route has been added...
	echo.
else
	echo.
	echo Exiting now with CodeReady Container started, but insecure route was not added
	echo so can't complete the demo setup for this project.
	echo.
	GOTO :EOF
)

if call :routeReady "kieserver" (
	echo The kieserver insecure route has been added...
	echo.
else
	echo.
	echo Exiting now with CodeReady Container started, but insecure route was not added
	echo so can't complete the demo setup for this project.
	echo.
	GOTO :EOF
)

if call :containerReady (
	echo.
	echo The container has started...
	echo.
) else (
	echo Exiting now with CodeReady Container started, but not sure if
	echo authoring environment is ready and did not install the demo project.
	echo.
	GOTO :EOF
)

echo.
echo =================================================================================
echo =                                                                               =
echo =  Login to Red Hat Process Automation Manager to explore process automation    =
echo =  development at:                                                              =
echo =                                                                               =
echo =  http://insecure-%OCP_APP%-rhpamcentr-%OCP_PRJ%.%HOST_APPS%               =
echo =                                                                               =
echo =    Log in: [ u:erics / p:redhatpam1! ]                                        =
echo =                                                                               =
echo =    Others:                                                                    =
echo =            [ u:kieserver / p:redhatpam1! ]                                    =
echo =            [ u:caseuser / p:redhatpam1! ]                                     =
echo =            [ u:casemanager / p:redhatpam1! ]                                  =
echo =            [ u:casesupplier / p:redhatpam1! ]                                 =
echo =                                                                               =
echo =  Explore the KieServer API docs here:                                         =
echo =                                                                               =
echo =  http://insecure-%OCP_APP%-kieserver-%OCP_PRJ%.%HOST_APPS%/docs           =
echo =                                                                               =
echo =================================================================================
echo.

