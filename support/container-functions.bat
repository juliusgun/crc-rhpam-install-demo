@ECHO OFF

REM Collection of container functions used in demos.
REM

REM prints the documentation for this script.
REM
:printDocs

	echo The default option is to run this using CodeReady Containers, an OpenShift Container
	echo Platform for your local machine. This host has been set by default in the variables at
	echo the top of this script. You can modify if needed for your own host and ports by mofifying
	echo these variables:
	echo.
	echo     HOST_IP=api.crc.testing
	echo     HOST_PORT=6443
	echo.
	echo It's also possible to install this project on any available OpenShift installation, just point
	echo this installer at your installation by passing an IP address of the hosting cluster:
	echo.
	echo    $ init.bat IP
	echo.
	echo IP could look like: 192.168.99.100
	echo.
	echo Both methodes are validated by the install scripts.
	echo.

:endPrintDocs


REM check for a valid passed IP address.
REM
:validateIP ipAddress [returnVariable]

	setlocal

	set "_return=1"

	echo %~1^| findstr /b /e /r "[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*" >nul

	if not errorlevel 1 for /f "tokens=1-4 delims=." %%a in ("%~1") do (
		if %%a gtr 0 if %%a lss 255 if %%b leq 255 if %%c leq 255 if %%d gtr 0 if %%d leq 254 set "_return=0"
	)

	endlocal & ( if not "%~2"=="" set "%~2=%_return%" ) & exit /b %_return%

:endValidateIP

REM check if instance of operator is ready.
REM
:instanceReady [returnVariable]

  setlocal

	set "_return=0"
	set count=0
	set created=false

	:loop
  call oc apply -f %SUPPORT_DIR%\kieapp-rhpam-authoring.yaml

	if %ERRORLEVEL% EQU 0 {
		set created=true
		GOTO :endLoop
	} 

	echo Operator is not finished installing yet... waiting to start an instance [ %count%s ]
	timeout /t 5 /nobreak > NUL
	set /A count=count+5

	if %count% GTR %DELAY% goto loop

	:endLoop

	if %created% EQU "false" {
	  set "_return=1"
	  echo.
		echo The business automation operator failed to install inside ${DELAY} sec,
		echo maybe try to increase the wait time by increasing  the value of variable
		echo 'DELAY' located at top of this script?
		echo.
		exit /b %_return% 
	}

	REM returning true.
	exit /b %_return%

	endlocal

:endInstanceReady

REM check if insecure route setup is ready.
REM
:routeReady [returnVariable]

  setlocal

	REM variable passed is either 'rhdmcentr' or 'kieserver' for insecure route.
	set server=%~1

	set "_return=0"
	set count=0
	set created=false

	:loop
	call oc apply -f %SUPPORT_DIR%\add-insecure-%server%-route.yaml

	if %ERRORLEVEL% EQU 0 {
		set created=true
		GOTO :endLoop
	} 

	echo Operator is not finished installing server '%server%' yet... waiting to add an insecure route  [ %count%s ]
	timeout /t 5 /nobreak > NUL
	set /A count=count+5

	if %count% GTR %DELAY% goto loop

	:endLoop

	if %created% EQU "false" {
	  set "_return=1"
	  echo.
		echo The insecure route failed to be added to business central instance inside ${DELAY} sec, 
		echo maybe try to increase the wait time by increasing the value of variable 'DELAY' located
		echo at top of this script?
		echo.
		exit /b %_return% 
	}

	REM returning true.
	exit /b %_return%

	endlocal

:endRouteReady

REM check if container is ready.
REM
:containerReady [returnVariable]

  setlocal

	set "_return=0"
	set count=0
	set created=false

	:loop
  set status=call curl -u %KIE_ADMIN_USER%:%KIE_ADMIN_PWD% --output NUL --write-out "%{http_code}" --silent --head --fail "http://insecure-%OCP_APP%-rhpamcentr-%OCP_PRJ%.%HOST_APPS%/rest/spaces"

	if %status% EQU 200 {
		set created=true
		GOTO :endLoop
	}

	echo Container has not started yet.. waiting on container [ %count%s ]
	timeout /t 5 /nobreak > NUL
	set /A count=count+5

	if %count% GTR %DELAY% goto loop

	:endLoop

	if %created% EQU "false" {
	  set "_return=1"
	  echo.
		echo The business central container failed to start inside %DELAY% sec,
		echo maybe try to increase the wait time by increasing  the value of variable
		echo 'DELAY' located at top of this script?
		echo.
		exit /b %_return%
	}

	REM returning true.
	exit /b %_return%

	endlocal

:endContainerReady

